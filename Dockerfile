FROM node:15.7.0-alpine3.11 

RUN apk add bash nano && apk add python3 python3-dev py3-pip build-base
RUN set -ex && apk --no-cache add sudo
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
# RUN npm install --production --silent && mv node_modules ../
COPY . .
EXPOSE 3000
RUN chown -R node /usr/src/app
USER node
CMD ["npm", "start"]
