var require = meteorInstall({"import":{"api":{"userinfo.js":function module(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                          //
// import/api/userinfo.js                                                                   //
//                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////
                                                                                            //
module.export({
  Userinfo: () => Userinfo,
  NeighbourInfo: () => NeighbourInfo
});
let Mongo;
module.link("meteor/mongo", {
  Mongo(v) {
    Mongo = v;
  }

}, 0);
let Meteor;
module.link("meteor/meteor", {
  Meteor(v) {
    Meteor = v;
  }

}, 1);
let check;
module.link("meteor/check", {
  check(v) {
    check = v;
  }

}, 2);
const Userinfo = new Mongo.Collection('userinfo');
const NeighbourInfo = new Mongo.Collection('neighbourInfo');

if (Meteor.isServer) {
  Meteor.publish('userinfo', function userinfoPublication() {
    return Userinfo.find({});
  });
  Meteor.publish('neighbourInfo', function () {
    return NeighbourInfo.find({});
  });
}

Meteor.methods({
  'user.insert'(title, device, latitude, longitude, country) {
    check(title, String);
    check(device, String);
    check(latitude, String);
    check(longitude, String);
    check(country, String);
    Userinfo.insert({
      title,
      device,
      latitude,
      longitude,
      country,
      createdAt: new Date()
    });
  },

  'user.remove'(userID) {
    check(userID, String);
    Userinfo.remove(userID);
  },

  'user.setEdit'(userID, setEdit) {
    check(userID, String);
    check(setEdit, Boolean);
    Userinfo.update(userID, {
      $set: {
        editing: setEdit
      }
    });
  },

  'user.update'(userID, title, devi) {
    check(userID, String);
    check(title, String);
    check(devi, String);
    Userinfo.update(userID, {
      $set: {
        title: title,
        device: devi
      }
    });
  },

  'user.addNeighb'(userID, nextUserID) {
    Userinfo.update(userID, {
      $push: {
        neighbour: nextUserID
      }
    });
  },

  'neighbour.insert'(userID, title, latitude, longitude) {
    check(userID, String);
    check(title, String);
    check(longitude, String);
    check(latitude, String);
    NeighbourInfo.insert({
      userID,
      title,
      latitude,
      longitude,
      neighbour: [],
      createdAt: new Date()
    });
  },

  'neighbour.add'(userID, neighbourID) {
    check(userID, String);
    check(neighbourID, String); //Method 1:
    // if(NeighbourInfo.find({userID: userID}).fetch()[0].neighbour.includes(neighbourID)){
    // Method 2:

    NeighbourInfo.update({
      userID: userID
    }, {
      $push: {
        neighbour: neighbourID
      }
    });
  },

  'neighbour.removeNeighbour'(user1, user2) {
    NeighbourInfo.update({
      "userID": user1
    }, {
      $pull: {
        "neighbour": user2
      }
    });
    NeighbourInfo.update({
      "userID": user2
    }, {
      $pull: {
        "neighbour": user1
      }
    });
  },

  'neighbour.remove'(userID) {
    check(userID, String);
    NeighbourInfo.update({
      "neighbour": userID
    }, {
      $pull: {
        "neighbour": userID
      }
    }, {
      multi: true
    });
    NeighbourInfo.remove({
      userID: userID
    });
  }

});
//////////////////////////////////////////////////////////////////////////////////////////////

}}},"server":{"main.js":function module(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                          //
// server/main.js                                                                           //
//                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////
                                                                                            //
let Meteor;
module.link("meteor/meteor", {
  Meteor(v) {
    Meteor = v;
  }

}, 0);
module.link("../import/api/userinfo.js");
Meteor.startup(() => {});
//////////////////////////////////////////////////////////////////////////////////////////////

}}},{
  "extensions": [
    ".js",
    ".json",
    ".ts",
    ".mjs"
  ]
});

var exports = require("/server/main.js");
//# sourceURL=meteor://💻app/app/app.js
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1ldGVvcjovL/CfkrthcHAvaW1wb3J0L2FwaS91c2VyaW5mby5qcyIsIm1ldGVvcjovL/CfkrthcHAvc2VydmVyL21haW4uanMiXSwibmFtZXMiOlsibW9kdWxlIiwiZXhwb3J0IiwiVXNlcmluZm8iLCJOZWlnaGJvdXJJbmZvIiwiTW9uZ28iLCJsaW5rIiwidiIsIk1ldGVvciIsImNoZWNrIiwiQ29sbGVjdGlvbiIsImlzU2VydmVyIiwicHVibGlzaCIsInVzZXJpbmZvUHVibGljYXRpb24iLCJmaW5kIiwibWV0aG9kcyIsInRpdGxlIiwiZGV2aWNlIiwibGF0aXR1ZGUiLCJsb25naXR1ZGUiLCJjb3VudHJ5IiwiU3RyaW5nIiwiaW5zZXJ0IiwiY3JlYXRlZEF0IiwiRGF0ZSIsInVzZXJJRCIsInJlbW92ZSIsInNldEVkaXQiLCJCb29sZWFuIiwidXBkYXRlIiwiJHNldCIsImVkaXRpbmciLCJkZXZpIiwibmV4dFVzZXJJRCIsIiRwdXNoIiwibmVpZ2hib3VyIiwibmVpZ2hib3VySUQiLCJ1c2VyMSIsInVzZXIyIiwiJHB1bGwiLCJtdWx0aSIsInN0YXJ0dXAiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUFBLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjO0FBQUNDLFVBQVEsRUFBQyxNQUFJQSxRQUFkO0FBQXVCQyxlQUFhLEVBQUMsTUFBSUE7QUFBekMsQ0FBZDtBQUF1RSxJQUFJQyxLQUFKO0FBQVVKLE1BQU0sQ0FBQ0ssSUFBUCxDQUFZLGNBQVosRUFBMkI7QUFBQ0QsT0FBSyxDQUFDRSxDQUFELEVBQUc7QUFBQ0YsU0FBSyxHQUFDRSxDQUFOO0FBQVE7O0FBQWxCLENBQTNCLEVBQStDLENBQS9DO0FBQWtELElBQUlDLE1BQUo7QUFBV1AsTUFBTSxDQUFDSyxJQUFQLENBQVksZUFBWixFQUE0QjtBQUFDRSxRQUFNLENBQUNELENBQUQsRUFBRztBQUFDQyxVQUFNLEdBQUNELENBQVA7QUFBUzs7QUFBcEIsQ0FBNUIsRUFBa0QsQ0FBbEQ7QUFBcUQsSUFBSUUsS0FBSjtBQUFVUixNQUFNLENBQUNLLElBQVAsQ0FBWSxjQUFaLEVBQTJCO0FBQUNHLE9BQUssQ0FBQ0YsQ0FBRCxFQUFHO0FBQUNFLFNBQUssR0FBQ0YsQ0FBTjtBQUFROztBQUFsQixDQUEzQixFQUErQyxDQUEvQztBQUl0TSxNQUFNSixRQUFRLEdBQUcsSUFBSUUsS0FBSyxDQUFDSyxVQUFWLENBQXFCLFVBQXJCLENBQWpCO0FBQ0EsTUFBTU4sYUFBYSxHQUFHLElBQUlDLEtBQUssQ0FBQ0ssVUFBVixDQUFxQixlQUFyQixDQUF0Qjs7QUFFUCxJQUFHRixNQUFNLENBQUNHLFFBQVYsRUFBbUI7QUFDZkgsUUFBTSxDQUFDSSxPQUFQLENBQWUsVUFBZixFQUEyQixTQUFTQyxtQkFBVCxHQUE4QjtBQUNyRCxXQUFPVixRQUFRLENBQUNXLElBQVQsQ0FBYyxFQUFkLENBQVA7QUFDSCxHQUZEO0FBR0FOLFFBQU0sQ0FBQ0ksT0FBUCxDQUFlLGVBQWYsRUFBZ0MsWUFBVTtBQUN0QyxXQUFPUixhQUFhLENBQUNVLElBQWQsQ0FBbUIsRUFBbkIsQ0FBUDtBQUNILEdBRkQ7QUFHSDs7QUFFRE4sTUFBTSxDQUFDTyxPQUFQLENBQWU7QUFDWCxnQkFBY0MsS0FBZCxFQUFxQkMsTUFBckIsRUFBNkJDLFFBQTdCLEVBQXVDQyxTQUF2QyxFQUFrREMsT0FBbEQsRUFBMEQ7QUFDdERYLFNBQUssQ0FBQ08sS0FBRCxFQUFRSyxNQUFSLENBQUw7QUFDQVosU0FBSyxDQUFDUSxNQUFELEVBQVNJLE1BQVQsQ0FBTDtBQUNBWixTQUFLLENBQUNTLFFBQUQsRUFBV0csTUFBWCxDQUFMO0FBQ0FaLFNBQUssQ0FBQ1UsU0FBRCxFQUFZRSxNQUFaLENBQUw7QUFDQVosU0FBSyxDQUFDVyxPQUFELEVBQVVDLE1BQVYsQ0FBTDtBQUVBbEIsWUFBUSxDQUFDbUIsTUFBVCxDQUFnQjtBQUNaTixXQURZO0FBRVpDLFlBRlk7QUFHWkMsY0FIWTtBQUlaQyxlQUpZO0FBS1pDLGFBTFk7QUFNWkcsZUFBUyxFQUFFLElBQUlDLElBQUo7QUFOQyxLQUFoQjtBQVFILEdBaEJVOztBQWlCWCxnQkFBY0MsTUFBZCxFQUFxQjtBQUNqQmhCLFNBQUssQ0FBQ2dCLE1BQUQsRUFBU0osTUFBVCxDQUFMO0FBQ0FsQixZQUFRLENBQUN1QixNQUFULENBQWdCRCxNQUFoQjtBQUNILEdBcEJVOztBQXFCWCxpQkFBZUEsTUFBZixFQUF1QkUsT0FBdkIsRUFBZ0M7QUFFNUJsQixTQUFLLENBQUNnQixNQUFELEVBQVNKLE1BQVQsQ0FBTDtBQUVBWixTQUFLLENBQUNrQixPQUFELEVBQVVDLE9BQVYsQ0FBTDtBQUNBekIsWUFBUSxDQUFDMEIsTUFBVCxDQUFnQkosTUFBaEIsRUFBd0I7QUFBRUssVUFBSSxFQUFFO0FBQUVDLGVBQU8sRUFBRUo7QUFBWDtBQUFSLEtBQXhCO0FBQ0gsR0EzQlU7O0FBNEJYLGdCQUFjRixNQUFkLEVBQXNCVCxLQUF0QixFQUE2QmdCLElBQTdCLEVBQWtDO0FBQzlCdkIsU0FBSyxDQUFDZ0IsTUFBRCxFQUFTSixNQUFULENBQUw7QUFDQVosU0FBSyxDQUFDTyxLQUFELEVBQVFLLE1BQVIsQ0FBTDtBQUNBWixTQUFLLENBQUN1QixJQUFELEVBQU9YLE1BQVAsQ0FBTDtBQUNBbEIsWUFBUSxDQUFDMEIsTUFBVCxDQUFnQkosTUFBaEIsRUFBd0I7QUFBRUssVUFBSSxFQUFFO0FBQUVkLGFBQUssRUFBRUEsS0FBVDtBQUFpQkMsY0FBTSxFQUFFZTtBQUF6QjtBQUFSLEtBQXhCO0FBQ0gsR0FqQ1U7O0FBa0NYLG1CQUFpQlAsTUFBakIsRUFBeUJRLFVBQXpCLEVBQW9DO0FBQ2hDOUIsWUFBUSxDQUFDMEIsTUFBVCxDQUFnQkosTUFBaEIsRUFBd0I7QUFBRVMsV0FBSyxFQUFFO0FBQUVDLGlCQUFTLEVBQUVGO0FBQWI7QUFBVCxLQUF4QjtBQUNILEdBcENVOztBQXFDWCxxQkFBbUJSLE1BQW5CLEVBQTJCVCxLQUEzQixFQUFrQ0UsUUFBbEMsRUFBNENDLFNBQTVDLEVBQXNEO0FBQ2xEVixTQUFLLENBQUNnQixNQUFELEVBQVNKLE1BQVQsQ0FBTDtBQUNBWixTQUFLLENBQUNPLEtBQUQsRUFBUUssTUFBUixDQUFMO0FBQ0FaLFNBQUssQ0FBQ1UsU0FBRCxFQUFZRSxNQUFaLENBQUw7QUFDQVosU0FBSyxDQUFDUyxRQUFELEVBQVdHLE1BQVgsQ0FBTDtBQUNEakIsaUJBQWEsQ0FBQ2tCLE1BQWQsQ0FBcUI7QUFBQ0csWUFBRDtBQUFRVCxXQUFSO0FBQWNFLGNBQWQ7QUFBd0JDLGVBQXhCO0FBQW1DZ0IsZUFBUyxFQUFDLEVBQTdDO0FBQWlEWixlQUFTLEVBQUUsSUFBSUMsSUFBSjtBQUE1RCxLQUFyQjtBQUNGLEdBM0NVOztBQTRDWCxrQkFBZ0JDLE1BQWhCLEVBQXdCVyxXQUF4QixFQUFvQztBQUNoQzNCLFNBQUssQ0FBQ2dCLE1BQUQsRUFBU0osTUFBVCxDQUFMO0FBQ0FaLFNBQUssQ0FBQzJCLFdBQUQsRUFBY2YsTUFBZCxDQUFMLENBRmdDLENBR2hDO0FBQ0E7QUFDQTs7QUFDQWpCLGlCQUFhLENBQUN5QixNQUFkLENBQXFCO0FBQUNKLFlBQU0sRUFBRUE7QUFBVCxLQUFyQixFQUF1QztBQUFDUyxXQUFLLEVBQUU7QUFBQ0MsaUJBQVMsRUFBRUM7QUFBWjtBQUFSLEtBQXZDO0FBQ0gsR0FuRFU7O0FBb0RYLDhCQUE0QkMsS0FBNUIsRUFBbUNDLEtBQW5DLEVBQXlDO0FBQ3JDbEMsaUJBQWEsQ0FBQ3lCLE1BQWQsQ0FBcUI7QUFBQyxnQkFBU1E7QUFBVixLQUFyQixFQUF1QztBQUFDRSxXQUFLLEVBQUU7QUFBQyxxQkFBYUQ7QUFBZDtBQUFSLEtBQXZDO0FBQ0FsQyxpQkFBYSxDQUFDeUIsTUFBZCxDQUFxQjtBQUFDLGdCQUFTUztBQUFWLEtBQXJCLEVBQXVDO0FBQUNDLFdBQUssRUFBRTtBQUFDLHFCQUFhRjtBQUFkO0FBQVIsS0FBdkM7QUFDSCxHQXZEVTs7QUF3RFgscUJBQW1CWixNQUFuQixFQUEwQjtBQUN0QmhCLFNBQUssQ0FBQ2dCLE1BQUQsRUFBU0osTUFBVCxDQUFMO0FBQ0FqQixpQkFBYSxDQUFDeUIsTUFBZCxDQUFxQjtBQUFDLG1CQUFZSjtBQUFiLEtBQXJCLEVBQTJDO0FBQUNjLFdBQUssRUFBQztBQUFDLHFCQUFhZDtBQUFkO0FBQVAsS0FBM0MsRUFBMEU7QUFBQ2UsV0FBSyxFQUFFO0FBQVIsS0FBMUU7QUFDQXBDLGlCQUFhLENBQUNzQixNQUFkLENBQXFCO0FBQUNELFlBQU0sRUFBRUE7QUFBVCxLQUFyQjtBQUNIOztBQTVEVSxDQUFmLEU7Ozs7Ozs7Ozs7O0FDaEJBLElBQUlqQixNQUFKO0FBQVdQLE1BQU0sQ0FBQ0ssSUFBUCxDQUFZLGVBQVosRUFBNEI7QUFBQ0UsUUFBTSxDQUFDRCxDQUFELEVBQUc7QUFBQ0MsVUFBTSxHQUFDRCxDQUFQO0FBQVM7O0FBQXBCLENBQTVCLEVBQWtELENBQWxEO0FBQXFETixNQUFNLENBQUNLLElBQVAsQ0FBWSwyQkFBWjtBQUdoRUUsTUFBTSxDQUFDaUMsT0FBUCxDQUFlLE1BQU0sQ0FDcEIsQ0FERCxFIiwiZmlsZSI6Ii9hcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb25nbyB9IGZyb20gXCJtZXRlb3IvbW9uZ29cIjtcbmltcG9ydCB7IE1ldGVvciB9IGZyb20gXCJtZXRlb3IvbWV0ZW9yXCI7XG5pbXBvcnQgeyBjaGVjayB9IGZyb20gJ21ldGVvci9jaGVjayc7XG5cbmV4cG9ydCBjb25zdCBVc2VyaW5mbyA9IG5ldyBNb25nby5Db2xsZWN0aW9uKCd1c2VyaW5mbycpO1xuZXhwb3J0IGNvbnN0IE5laWdoYm91ckluZm8gPSBuZXcgTW9uZ28uQ29sbGVjdGlvbignbmVpZ2hib3VySW5mbycpO1xuXG5pZihNZXRlb3IuaXNTZXJ2ZXIpe1xuICAgIE1ldGVvci5wdWJsaXNoKCd1c2VyaW5mbycsIGZ1bmN0aW9uIHVzZXJpbmZvUHVibGljYXRpb24oKXtcbiAgICAgICAgcmV0dXJuIFVzZXJpbmZvLmZpbmQoe30pO1xuICAgIH0pXG4gICAgTWV0ZW9yLnB1Ymxpc2goJ25laWdoYm91ckluZm8nLCBmdW5jdGlvbigpe1xuICAgICAgICByZXR1cm4gTmVpZ2hib3VySW5mby5maW5kKHt9KTtcbiAgICB9KVxufVxuXG5NZXRlb3IubWV0aG9kcyh7XG4gICAgJ3VzZXIuaW5zZXJ0Jyh0aXRsZSwgZGV2aWNlICxsYXRpdHVkZSwgbG9uZ2l0dWRlLCBjb3VudHJ5KXtcbiAgICAgICAgY2hlY2sodGl0bGUsIFN0cmluZyk7XG4gICAgICAgIGNoZWNrKGRldmljZSwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2sobGF0aXR1ZGUsIFN0cmluZyk7XG4gICAgICAgIGNoZWNrKGxvbmdpdHVkZSwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2soY291bnRyeSwgU3RyaW5nKTtcblxuICAgICAgICBVc2VyaW5mby5pbnNlcnQoe1xuICAgICAgICAgICAgdGl0bGUsXG4gICAgICAgICAgICBkZXZpY2UsXG4gICAgICAgICAgICBsYXRpdHVkZSxcbiAgICAgICAgICAgIGxvbmdpdHVkZSxcbiAgICAgICAgICAgIGNvdW50cnksXG4gICAgICAgICAgICBjcmVhdGVkQXQ6IG5ldyBEYXRlKCksXG4gICAgICAgIH0pO1xuICAgIH0sXG4gICAgJ3VzZXIucmVtb3ZlJyh1c2VySUQpe1xuICAgICAgICBjaGVjayh1c2VySUQsIFN0cmluZyk7XG4gICAgICAgIFVzZXJpbmZvLnJlbW92ZSh1c2VySUQpO1xuICAgIH0sXG4gICAgJ3VzZXIuc2V0RWRpdCcodXNlcklELCBzZXRFZGl0KSB7XG5cbiAgICAgICAgY2hlY2sodXNlcklELCBTdHJpbmcpO1xuICAgIFxuICAgICAgICBjaGVjayhzZXRFZGl0LCBCb29sZWFuKTtcbiAgICAgICAgVXNlcmluZm8udXBkYXRlKHVzZXJJRCwgeyAkc2V0OiB7IGVkaXRpbmc6IHNldEVkaXQgfSB9KTtcbiAgICB9LFxuICAgICd1c2VyLnVwZGF0ZScodXNlcklELCB0aXRsZSwgZGV2aSl7XG4gICAgICAgIGNoZWNrKHVzZXJJRCwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2sodGl0bGUsIFN0cmluZyk7XG4gICAgICAgIGNoZWNrKGRldmksIFN0cmluZyk7XG4gICAgICAgIFVzZXJpbmZvLnVwZGF0ZSh1c2VySUQsIHsgJHNldDogeyB0aXRsZTogdGl0bGUsICBkZXZpY2U6IGRldml9IH0gKTtcbiAgICB9LFxuICAgICd1c2VyLmFkZE5laWdoYicodXNlcklELCBuZXh0VXNlcklEKXtcbiAgICAgICAgVXNlcmluZm8udXBkYXRlKHVzZXJJRCwgeyAkcHVzaDogeyBuZWlnaGJvdXI6IG5leHRVc2VySUR9fSk7XG4gICAgfSxcbiAgICAnbmVpZ2hib3VyLmluc2VydCcodXNlcklELCB0aXRsZSwgbGF0aXR1ZGUsIGxvbmdpdHVkZSl7XG4gICAgICAgIGNoZWNrKHVzZXJJRCwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2sodGl0bGUsIFN0cmluZyk7XG4gICAgICAgIGNoZWNrKGxvbmdpdHVkZSwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2sobGF0aXR1ZGUsIFN0cmluZyk7XG4gICAgICAgTmVpZ2hib3VySW5mby5pbnNlcnQoe3VzZXJJRCx0aXRsZSxsYXRpdHVkZSwgbG9uZ2l0dWRlLCBuZWlnaGJvdXI6W10sIGNyZWF0ZWRBdDogbmV3IERhdGUoKX0pXG4gICAgfSxcbiAgICAnbmVpZ2hib3VyLmFkZCcodXNlcklELCBuZWlnaGJvdXJJRCl7XG4gICAgICAgIGNoZWNrKHVzZXJJRCwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2sobmVpZ2hib3VySUQsIFN0cmluZyk7XG4gICAgICAgIC8vTWV0aG9kIDE6XG4gICAgICAgIC8vIGlmKE5laWdoYm91ckluZm8uZmluZCh7dXNlcklEOiB1c2VySUR9KS5mZXRjaCgpWzBdLm5laWdoYm91ci5pbmNsdWRlcyhuZWlnaGJvdXJJRCkpe1xuICAgICAgICAvLyBNZXRob2QgMjpcbiAgICAgICAgTmVpZ2hib3VySW5mby51cGRhdGUoe3VzZXJJRDogdXNlcklEfSwgeyRwdXNoOiB7bmVpZ2hib3VyOiBuZWlnaGJvdXJJRH19KTtcbiAgICB9LFxuICAgICduZWlnaGJvdXIucmVtb3ZlTmVpZ2hib3VyJyh1c2VyMSwgdXNlcjIpe1xuICAgICAgICBOZWlnaGJvdXJJbmZvLnVwZGF0ZSh7XCJ1c2VySURcIjp1c2VyMX0sIHskcHVsbDoge1wibmVpZ2hib3VyXCI6IHVzZXIyfX0pO1xuICAgICAgICBOZWlnaGJvdXJJbmZvLnVwZGF0ZSh7XCJ1c2VySURcIjp1c2VyMn0sIHskcHVsbDoge1wibmVpZ2hib3VyXCI6IHVzZXIxfX0pO1xuICAgIH0sXG4gICAgJ25laWdoYm91ci5yZW1vdmUnKHVzZXJJRCl7XG4gICAgICAgIGNoZWNrKHVzZXJJRCwgU3RyaW5nKTtcbiAgICAgICAgTmVpZ2hib3VySW5mby51cGRhdGUoe1wibmVpZ2hib3VyXCI6dXNlcklEfSwgeyRwdWxsOntcIm5laWdoYm91clwiOiB1c2VySUR9fSwge211bHRpOiB0cnVlfSk7XG4gICAgICAgIE5laWdoYm91ckluZm8ucmVtb3ZlKHt1c2VySUQ6IHVzZXJJRH0pO1xuICAgIH0sXG59KSIsImltcG9ydCB7IE1ldGVvciB9IGZyb20gJ21ldGVvci9tZXRlb3InO1xuaW1wb3J0ICcuLi9pbXBvcnQvYXBpL3VzZXJpbmZvLmpzJztcblxuTWV0ZW9yLnN0YXJ0dXAoKCkgPT4ge1xufSk7XG4iXX0=
