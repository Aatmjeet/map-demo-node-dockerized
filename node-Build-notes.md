# Build command 
## meteor build --directory _____

# Go the directory lets say ~/Desktop/NodeBuild
## then we have a bundle here, bundle has the compmentent.
## cd bundle we have a README, which contains the install steps.

## Make sure to upgrade node version to >14 as meteor only supports node >14

## A key thing to note for our build is that fibres package will give install errors,
### To fix go to /bundle/programs/server/node_modules/fibres and then npm install.

### Also fibres package doesn't support node 16, the current stable.
### So downgrade to node 15 using, sudo n 15

## Reminder: don't forget to add/export db-Url, ROOT_URL and PORT.
### db-URL format 'mongodb://localhost/${dbname}'

## After installing, 
### Add a package.json using nom start.
### npm install --save nodemon
### ctrl+shift+p and add docker files.

## POrt forwarding put same ports.
## change port to 80 for ease
### port on 80 will have permission issues,
### use  sudo apt-get install libcap2-bin
### and sudo setcap cap_net_bind_service=+ep `readlink -f \`which node\``


## After installing on docker, I had troubles installing packages
### So 1st downgrade node version to 15
### to get into root use "Sudo docker exec -it --user root {container hash} bash
### After installing add MONGO_URL='mongodb:mongodb:27017/mapdemo'


## Also just docker commands to re-build containers
### sudo docker stop $(sudo docker ps -a)
### sudo docker rm $(sudo docker ps -a)
### sudo docker rmi $(sudo docker images)
### sudo docker volume rm $(sudo docker volume ls)

## In airtel routes port 443, 80 are blocked and hence i cant use certbot.
### This is because, certbot requires a domain name and domain name require just the IP address without the port.
